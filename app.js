const express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path'),
    favicon = require('serve-favicon'),
    cookieParser = require('cookie-parser'),
    logger = require('morgan'),
    nodemailer = require('nodemailer'),
    session = require('express-session'),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy,
    expressValidator = require('express-validator'),
    multer = require('multer'),
    upload = multer({dest: './uploads'}),
    flash = require('connect-flash'),
    bcrypt = require('bcryptjs'),
    mongo = require('mongodb');
    mongoose = require('mongoose');
    db = mongoose.connection,
    app = express();

// Define routes
const routes = require('./routes/index'),
    users = require('./routes/users'),
    blog = require('./routes/blog'),
    contact = require('./routes/contact');

app.locals.moment = require('moment');

// Define variables
app.set('port', (process.env.PORT || 3000));
app.set('views', path.join(__dirname, 'public/templates'));
app.set('view engine', 'pug');

// MIDDLEWARES
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.resolve(__dirname, 'public')));
app.use(flash());

// Handle sessions
app.use(session({
    secret: 'secret',
    saveUninitialized: true,
    resave: true
}));

// Passport
app.use(passport.initialize());
app.use(passport.session());

// validator
app.use(expressValidator({
    errorformatter(param, msg, value) {
        const namespace = param.split('.'),
            root = namespace.shift(),
            formParam = root;
        
        while(namespace.length) {
            formParam += `[${namespace.shift()}]`;
        }
        return {
            param: formParam,
            msg: msg,
            value: value
        };
    }
}));

app.use(function (req, res, next) {
  res.locals.messages = require('express-messages')(req, res);
  next();
});

app.get('*', (req, res, next) => {
    res.locals.user = req.user || null;
    next();
});

app.use('/', routes);
app.use('/users', users);
app.use('/blog', blog);
app.use('/contact', contact);


// catch 404 and forward to error handler
app.use(function(req, res, next) {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});
  
// error handlers
  
// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}
  
// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});

  
app.listen(app.get('port'), function() {
    console.log('Server started on port ' + app.get('port'));
});