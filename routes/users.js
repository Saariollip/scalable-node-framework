const express = require('express'),
    router = express.Router(),
    multer = require('multer'),
    upload = multer({dest: './uploads'}),
    passport = require('passport'),
    LocalStrategy = require('passport-local').Strategy;

const User = require('../models/user');

router.get('/', (req, res, next) => {
    res.render('index', { title: 'Users' });
});

router.get('/register', (req, res, next) => {
    res.render('register', { title: 'Register'});
});

router.get('/login', (req, res, next) => {
    res.render('login', { title: 'Login'});
});

passport.serializeUser(function(user, done) {
    done(null, user.id);
  });
  
passport.deserializeUser(function(id, done) {
    User.findById(id, function(err, user) {
      done(err, user);
    });
});

router.post('/login',
    passport.authenticate('local', {failureRedirect: '/users/login', failureFlash: 'Invalid username or password'}),
    (req, res) => {
        req.flash('success', 'You are now logged in');
    res.redirect('/');
});

passport.use(new LocalStrategy((username, password, done) => {
    User.getUserByUsername(username, (err, user) => {
        if (err) throw err;
        if (!user) {
            return done(null, false, {message: 'Unknown User'});
        }

        User.comparePassword(password, user.password, (err, isMatch) => {
            if (err) return done(err);
            if (isMatch) {
                return done(null, user);
            } else {
                return done(null, false, {message: 'Invalid Password'});
            }
        });
    });
}));

router.post('/register', upload.single('profileimage'), (req, res, next) => {
    const { name, email, username, password, password2 }  = req.body;
    
    let profileimage = '';
    if(req.file) {
        profileimage = req.file.filename;
    } else {
        profileimage = 'noimage.jpg';
    }
    
    // Validation
    req.checkBody('name', 'Name field is required').notEmpty();
    req.checkBody('email', 'Email field is required').notEmpty();
    req.checkBody('email', 'Email field is valid').isEmail();
    req.checkBody('username', 'Username field is required').notEmpty();
    req.checkBody('password', 'Password field is required').notEmpty();
    req.checkBody('password2', 'Passwords do not match').equals(req.body.password);

    const errors = req.validationErrors();

    if(errors) {
        res.render('register', {
            errors
        });
    } else {
        const newUser = new User({
            name,
            email,
            username,
            password,
            profileimage
        });

        User.createUser(newUser, (err, user) => {
            if (err) throw err;
            console.log(user);
        });

        req.flash('success', 'You are now registered and can login');

        res.location('/');
        res.redirect('/');
    }
});

router.get('/logout', (req, res) => {
    req.logout();
    req.flash('success', 'You are now logged out');
    res.redirect('/users/login')
});

module.exports = router;