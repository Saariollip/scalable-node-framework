const express = require('express');
const router = express.Router();
const multer = require('multer');
const upload = multer({ dest: 'uploads/ '});

const mongo = require('mongodb');
const db = require('monk')('localhost/nodeblog');

router.get('/', (req, res, next) => {
    const posts = db.get('posts');
    posts.find({}, {}, (err, posts) => {
        console.log(posts);
        res.render('blog/index', {posts: posts })
    });
});

router.get('/add', (req, res, next) => {
    const categories = db.get('categories');

    categories.find({}, {}, (err, categories ) => {
        res.render('addpost', {
            'title': 'Add Post',
            'categories': cattegories
        });
    })

});

router.post('/add', upload.single('mainimage'), (req, res, next) => {
    // Get Form values
    const title = req.body.title;
    const category = req.body.category;
    const body = req.body.body;
    const author = req.body.author;
    const date = new Date(); 

    if (req.file) {
        const mainimage = req.file.filename;
    } else {
        const mainimage = 'noimage.jpg';
    }

    req.checkBody('title', 'Title field is require').notEmpty();
    req.checkBody('body', 'Body field is required').notEmpty();

    const errors = req.validationErrors();

    if (error) {
        res.render('addpost', {
            "errors": errors
        });
    } else {
        const posts = db.get('posts');

        posts.insert({
            "title": title,
            "body": body,
            "category": category,
            "date": date,
            "author": author,
            "mainimage": mainimage
        }, (err, post) => {
            if (err) {
                res.send(err);
            } else {
                req.flash('success', 'Post Added');
                res.location('/');
                res.redirect('/');
            }
        })
    }
});


module.exports = router;